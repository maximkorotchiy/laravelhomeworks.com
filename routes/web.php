<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//

//Route::group(['prefix' => 'users'], function () {
//    Route::get('/', 'UserController@users');
//    Route::get('/{id}', 'UserController@profile');
//})->name('users.');

Route::prefix('users')
    ->name('users')
    ->group(function () {
        Route::get('/', 'UserController@users');
        Route::get('/{id?}', 'UserController@profile');
});

Route::get('/', 'UserController@main')->name('/');
Route::get('/about_project', 'UserController@aboutProject')->name('aboutProject');
