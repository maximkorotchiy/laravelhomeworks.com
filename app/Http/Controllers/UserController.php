<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
//    public $users = [
//        [
//            'image' => '',
//            'full_name' => 'Максим Коротчий',
//            'country' => 'Украина',
//            'specialty' => 'Программист',
//            'experience' => '0',
//        ],
//        [
//            'image' => 'alexander.jpg',
//            'full_name' => 'Александр Мартыненко',
//            'country' => 'Украина',
//            'specialty' => 'Слесарь',
//            'experience' => '20',
//        ],
//        [
//            'image' => 'fedor.jpg',
//            'full_name' => 'Федор Бакай',
//            'country' => 'Украина',
//            'specialty' => 'Архитектор',
//            'experience' => '5',
//        ]
//    ];


    public function main () {
        app()->setLocale('ru');

        return view('index');
    }

    public function users () {
        app()->setLocale('ru');
//        $users = $this->users;

//        return view('users.index', compact('users'));

        $users = DB::table('users')->get();
//        foreach ($users as $user) {
//            dump($user);
//        }
//        $users = $users->toArray();
        return view('users.index', compact('users'));
    }

    public function profile($id) {
        app()->setLocale('ru');
//        $user = $this->users[$id]; // this is array

        $user = DB::table('users')->where('id', '=', $id)->first();
//        dump($user);
//        foreach ($user as $i) {
//            echo $i->full_name;
//
//        }
//        dd($user);
        return view('users.profile', compact('user'));
    }

    public function aboutProject() {
        app()->setLocale('ru');

        return view('about_project');
    }
}
