<footer class="py-4 bg-dark {{ Request::is('/') ? 'fixed-bottom' : '' }} {{--fixed-bottom--}} text-white-50">
    <div class="container text-center">
        <small>2020 BestForum</small>
    </div>
</footer>
