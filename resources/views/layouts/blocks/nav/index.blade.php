<nav class="navbar sticky-top navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="/">BestForum</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class=" collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav mr-5 pr-4">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('/') }}">@lang('navbar.main')</a>
            </li>
            <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('users') }}">@lang('navbar.users')</a>
            </li>
            <li class="nav-item {{ Request::is('about_project') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('aboutProject') }}">@lang('navbar.about_project')</a>
            </li>
        </ul>
    </div>
</nav>
