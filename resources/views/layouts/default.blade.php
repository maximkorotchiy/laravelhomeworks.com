<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <title>@yield('title', 'BestForum')</title>
        <style>
            ._main_block {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
            }
            @media screen and (min-width: 576px) {
                .navbar-collapse {
                    display: flex;
                    justify-content: center;
                }
            }
            @media screen and (max-width: 576px) {
                .navbar-collapse {
                    display: block;
                }
            }
        </style>
    </head>

    <body>
        @include('layouts.blocks.nav.index')

        @yield('content')

        @include('layouts.blocks.footer.index')

        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>

