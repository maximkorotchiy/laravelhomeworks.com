@extends('layouts.default')

@section('title', 'Users')

{{--@section('is_active', 'active')--}}

@section('content')


        <div class=" mt-4 container">
            <div class="row">
                @forelse($users as $user)
                    <div class="col-md-4 col-lg-3 mb-4">
                        <div class="card">
                            <div class="" style="height: 200px;">
                                <img
                                    @if(!empty($user->image))
                                        src="{{ asset("img/$user->image") }}"
                                    @else
                                        src="{{ asset("img/noimg.png") }}"
                                    @endif
                                    class="card-img-top"
                                    alt="err"
                                >
                            </div>
                            <div class="card-body mt-2">
{{--                                <h5 class="card-title">{{ $user['full_name'] }}</h5>--}}
                                <h5 class="card-title">{{ $user->full_name }}</h5>
                                <p class="card-text">
                                    <span><b>Специальность:</b></span> <span>{{ $user->specialty }}</span>
                                </p>
{{--                                <a href="/users/{{ $loop->index }}" class="float-right btn btn-primary">Профиль &raquo</a>--}}
                                    <a href="/users/{{ $user->id }}" class="float-right btn btn-primary">Профиль &raquo</a>

                            </div>
                        </div>
                    </div>
                @empty
                    Не найдено ни одного пользователя
                @endforelse


            </div>
        </div>



@endsection
