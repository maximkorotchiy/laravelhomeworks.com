@extends('layouts.default')


@section('title', $user->full_name)

@section('content')

    @php(var_dump($user))
    <div class=" mt-4 container">
        <div class="row justify-content-end">
            <div class="col-12 col-md-6">
                <div class="row no-gutters">
                    <div class="col-6">
                        <img
                            @if(!empty($user->image))
                                src="{{ asset("img/$user->image") }}"
                            @else
                                src="{{ asset("img/noimg.png") }}"
                            @endif
                            class="card-img-top"
                            alt="err"
                        >
                    </div>
                    <div class="col-6 justify-content-center d-flex align-items-center">
                        <b>{{ $user->full_name }}</b>
                    </div>
                </div>
                <hr>
                <p>Страна: <span>{{ $user->country }}</span></p>
                <p>Специальность: <span>{{ $user->specialty }}</span></p>
                <p>Опыт работы: <span>{{ $user->experience }}</span> лет</p>
            </div>
        </div>
    </div>

@endsection
